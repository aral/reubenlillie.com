---
title: Repertoire
shortTitle: Rep
layout: layouts/repertoire
tags: singing
weight: 2
description: Here’s a sample of my stage and concert work as a classically trained tenor and recitalist.
---

I’m a classically trained tenor, currently exploring lyric and lyrico-dramatic repertoire. I’m also comfortable with a number of contemporary vocal techniques.

(From 2007 to 2019, I performed as a [baritone](/baritone-rep/).)

This page lists some of my [stage](#stage) and [concert](#concert) work.

[Have voice, will travel](/contact/). What else would you like me to sing?
