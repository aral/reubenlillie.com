---
title: Program Bio
shortTitle: Bio
tags: singing
weight: 1
description: Here’s a copy of my headshot and singer program bio.
---

<picture class="float float-left" style="shape-outside:circle(50%);">
  <source srcset="/includes/assets/img/headshot.webp" type="image/webp">
  <source srcset="/includes/assets/img/headshot.jpg" type="image/jpeg">
  <img src="/includes/assets/img/headshot.jpg" class="circular headshot" alt="Reuben’s headshot" width="300" height="300">
</picture>

Reuben L. Lillie was born and raised in the hills of Greenville, Pennsylvania.

Known for his “virile sound” and “reliable technique” (_Opera News_), Reuben’s [stage](/rep/#stage "Check out my stage repertoire") appearances include DuPage Opera Theatre’s _The Merry Widow_ (Kromov), Chicago Summer Opera’s _Le comte Ory_ (Raimbaud), Des Moines Metro Opera’s _Galileo Galilei_ (Salviati), Vero Beach Opera’s _Die Fledermaus_ (Frank), Sugar Creek Opera’s _The Magic Flute_ (Papageno), and the Santa Fe Opera’s world premiere of _Oscar_ (Jury Foreman).

In [concert](/rep/#concert "Check out my concert repertoire") Reuben has sung with a variety of chamber music groups including Fourth Coast Ensemble, Gaudete Brass Quintet, and Civic Orchestra of Chicago.

Reuben is a graduate of Olivet Nazarene University (where he now serves as an [adjunct professor](/teaching/ "Learn more about my teaching")), the Chicago College of Performing Arts (CCPA) at Roosevelt University, and McCormick Theological Seminary.

Reuben performed the Chicago premiere for the orchestrated version of Vaughan Williams’s _Songs of Travel_ at Orchestra Hall, receiving subsequent broadcasts on WFMT. He has also been a Central Region Finalist to the Metropolitan Opera National Council.

Reuben, his wife Stephanie, their son Corban, and their cat Sgt. Tibbs live in Chicago’s Loop.
