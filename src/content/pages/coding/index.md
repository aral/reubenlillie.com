---
title: Coding
tags: activities
syntaxHighlighting: true
weight: 4
description: I taught myself how to code in 2014. Now I teach ethical web development at the college level. Let’s make something worthwhile together!
---

```js
var introduce = function (name) {
	return 'Hi, I’m ' + name + '.'
}

introduce('Reuben')
```

I taught myself how to code in the summer of 2014 while I was under an opera contract between semesters at seminary. The rehearsal schedule was grueling, and I experienced some pretty bad insomnia. I needed something to help me wind down from singing—something quiet yet productive, still creative yet altogether different.

Since then, I’ve also delved into graphic design and branding practices. I specialize in web ethics: building fast, lightweight, accessible, and secure sites and applications. I’m an outspoken advocate for vanilla JavaScript and JAMstack techniques, helping to found the [JAMstack Chicago MeetUp®](https://www.netlify.com/). And I’ve created an interdisciplinary course for the Olivet Nazarene University Department of Computer Science and Emerging Technologies called “[Ethical Web Development](/comp-496/).”

This site, for example, is built with the static site generator [Eleventy](https://11ty.dev/) and hosted on [Netlify](https://www.netlify.com/).

[Let’s make something worthwhile together](/contact/).
