---
title: Copyright Notice
shortTitle: Copyright
tags: policies
weight: 1
description: Copyright and licensing policies for all content and source code of my personal website–written in plain language.
---

I value creativity. And I want you to feel free to improve upon my work.

This copyright notice applies to all content and source code on my site ([https://reubenlillie.com/](/)).

## Copyrighted Content

Unless otherwise noted, content on this site is licensed under the [Creative Commons Attribution-ShareAlike 4.0 International license][cc-by-sa].

You are free to copy and redistribute the material in any medium or format. You are also free to remix, transform, and build upon the material for any purpose, even commercially. I will not revoke these terms as long as you follow the terms of this license.

You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.

You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

For further permissions, please [contact me][contact].

## Source Code License

The source code for this site is licensed under the [MIT License][mit]. A copy of the license is available in the source code repository on [GitLab][mit].

[cc-by-sa]: https://creativecommons.org/licenses/by-sa/4.0/
[contact]: /contact/
[mit]: https://gitlab.com/reubenlillie/reubenlillie.com/blob/master/LICENSE/
