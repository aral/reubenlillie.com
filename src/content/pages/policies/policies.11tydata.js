/**
 * @file Contains metadata common to all policy pages, to reduce repetition
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/**
 * Directory data module
 * @module content/policies
 * @see {@link https://www.11ty.dev/docs/data-template-dir/ Template and directory data files in 11ty}
 */
module.exports = {
  date: 'Last Modified'
}
