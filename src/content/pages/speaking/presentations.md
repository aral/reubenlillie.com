---
title: Presentations
layout: layouts/presentations
tags: speaking
weight: 1
description: Here’s a sample of presentations I’ve given at universities, conferences, houses of worship, and MeetUps.
---

