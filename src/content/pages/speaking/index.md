---
title: Speaking
tags: activities
weight: 2
description: "I’m available for a variety of public speaking engagements: speeches, preaching, master classes, training sessions, you name it."
---

My formal disciplines are philosophy, theology, and music.

In addition to my work as a [singer](/singing/), I [teach](/teaching/) at Olivet Nazarene University and serve as a minister in the [Church of the Nazarene][church].

Since 2014, I’ve also been freelancing as a [web developer](/coding/). I’m involved in the open source community, focusing on ethical software development practices.

I’m available to preach or speak on a variety of subjects. Here are a few of my favorites (in no particular order):

* Vocation
* Social epistemology
* Acting for singers
* Sanctification
* Lifelong learning
* Vanilla JavaScript
* Liturgy
* Bylaws and policy making
* Preaching
* JAMstack web development
* Spiritual formation
* Pedagogy
* Wesleyan-Holiness theology, history, and practice
* Faith and ethics
* Urban/rural dynamics

[Let’s keep the conversation going](/contact/).

[church]: https://loopnaz.org/
