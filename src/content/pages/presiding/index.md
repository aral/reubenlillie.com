---
title: Presiding
tags: activities
weight: 6
description: Don’t you wish you knew someone who knows Robert’s Rules of Order really well? I think you found another reason to get to know me &hellip;
---

I’m an expert in [_Robert’s Rules of Order_](https://robertsrules.com/) and a member of the [National Association of Parliamentarians][nap].

I grew up attending board meetings with my parents, who instilled in me the value of parliamentary procedure for getting business done fairly and efficiently. Now I serve on a number of not-for-profit boards and committees. I believe in creating safe spaces where people feel free to share their greatest concerns and best ideas. As a parliamentarian, I’m available to assist organizations, especially not-for-profits, with any of the following services (in no particular order).

* Professional presiding officer
* Convention parliamentarian
* Bylaws consultant
* Advisor to the officers and board of directors
* Trainer in parliamentary procedure for organizations or individuals
* Election supervisor
* Meeting strategist
* Script writer
* Expert witness

If you or your organization needs advice on parliamentary procedure, then please [contact me](/contact/). I’m happy to help.

[nap]: https://parliamentarians.org/
