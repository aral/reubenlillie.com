---
title: Thank you for contacting me!
layout: layouts/page
eleventyExcludeFromCollections: true
---

I got your message, and I’ll respond soon.

Feel free to keep looking around:

* Go back to the [homepage](/)
* [Send me another message](/contact/)
