---
title: Writing
tags: activities
weight: 3
description: Like what you’ve read so far? I’ve got more where that came from. Wish I’d write about something else? I will if you ask me.
---

I love interdisciplinary research. I also like to balance my scholarship with writing for the general public.

My primary fields of study are philosophy, theology, and music. Most of my research deals with group dynamics, especially how individuals and groups aspire to become better people and make the world a better place. Currently, I’m exploring the topics of togetherness and moral transformation, particularly how our differences either contribute to or conflict with those aspirations. But I’ll write about almost anything [if you ask me](/contact/).

You can sample some of my [academic writing](/papers/) on this site, at [Olivet Digital Commons][digital-commons], and at [Academia.edu](https://olivet.academia.edu/ReubenLillie).

I have three book-length projects in the works. One looks at modern church history through the lenses of race, gender, and class. Another discusses the relationship between ecology, economics, and ecclesiology. And I’m experimenting with a conversational preaching method that incorporates lessons I’ve learned from jazz and improvizational theater.

[digital-commons]: https://digitalcommons.olivet.edu/do/search/?q=author_lname%3A%22Lillie%22%20AND%20author_fname%3A%22Reuben%22&start=0&context=1036391&sort=date_desc
