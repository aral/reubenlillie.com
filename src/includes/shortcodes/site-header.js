/**
 * @file Defines a shortcode for the page header
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#javascript-template-functions JavaScript template functions in 11ty}
 */

/**
 * A JavaScript Template module for the page header
 * @module includes/shortcodes/site-header
 * @param {Object} eleventyConfig 11ty’s Config API
 */
module.exports = function (eleventyConfig) {

  /**
   * The page header markup
   * @method
   * @name siteHeader
   * @example `${this.siteHeader(data)}`
   * @param {Object} data 11ty’s data object
   * @return {String} HTML template literal
   */
  eleventyConfig.addShortcode('siteHeader', function (data) {
    return `<header id="site_header" class="grid-column-full-bleed xx-large">
        ${this.gridNav(data.collections.headerMenu, data.page)}
      </header>`
  })

}
