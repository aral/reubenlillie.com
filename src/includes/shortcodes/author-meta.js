/**
 * @file Defines a shortcode for the author metadata
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#javascript-template-functions JavaScript template functions in 11ty}
 */

/**
 * A JavaScript Template module for the author metadata
 * @module includes/shortcodes/author-meta
 * @param {Object} eleventyConfig 11ty’s Config API
 */
module.exports = function (eleventyConfig) {

  /**
   * HTML author metadata
   * @method
   * @name titleTag
   * @param {Object} data 11ty’s data object
   * @return {String} HTML template literal
   * @example `${this.authorMeta(data)}`
   */
  eleventyConfig.addShortcode('authorMeta', function (data) {
    return `<meta name="author"
      content="
      ${data.site.author.name.fullName
        ? data.site.author.name.fullName
        : data.site.copyright.holder
          ? data.site.copyright.holder
          : data.site.title
      }"
    >`
  })

}
