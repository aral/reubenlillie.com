/**
 * @file Defines a shortcode for displaying navigation menus on a grid
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#javascript-template-functions 11ty docs}
 */

/**
 * A JavaScript Template module for a navigation menu
 * @module includes/shortcodes/grid-nav
 * @param {Object} eleventyConfig 11ty’s Config API
 */
module.exports = function (eleventyConfig) {

  /**
   * Navigation menu markup
   * @method
   * @name gridNav
   * @param {Array} collection 11ty collection to map over
   * @param {Object} page The current 11ty `page` object
   * @return {String} HTML template literal
   * @example `${this.gridNav(data.collections.policies, data.page)}`
   * @see {@link https://www.11ty.dev/docs/collections/ 11ty docs}
   */
  eleventyConfig.addShortcode('gridNav', function (collection, page) {
    return typeof collection !== 'undefined' && collection.length > 0
      ? `<nav class="grid">
        ${collection
          .sort(function (a, b) {
            return a.data.weight - b.data.weight
          })
          .map(function (item) {
            return (page.url === item.url)
              ? `<span class="nav-item active bold" title="This is the current page.">
                  ${item.data.shortTitle ? item.data.shortTitle : item.data.title}
                </span>`
              : `<a href="${item.url}" class="nav-item bold">
                  ${item.data.shortTitle ? item.data.shortTitle : item.data.title}
                </a>`
          })
          .join("\n")
        }
      </nav>`
      : ''
  })

}
