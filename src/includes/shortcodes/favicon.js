/**
 * @file Defines a shortcode to load a favicon for the user’s platform
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/**
 * An 11ty univeral shortcode
 * @module includes/shortcode/favicon
 * @param {Object} eleventyConfig 11ty’s Config API
 * @see {@link https://www.11ty.dev/docs/filters/ 11ty docs}
 */
module.exports = function (eleventyConfig) {

  /**
   * Load the appropriate favicon
   * @method
   * @name favicon
   * @param {Object} data 11ty’s data object
   * @return {String} HTML template literal
   * @example `${this.favicon(data)}`
   * @see {@link https://realfavicongenerator.net/ Favicon Generator}
   */
  eleventyConfig.addShortcode('favicon', function (data) {
    return `${this.fileToString('/favicons/html_code.html')}`
  })

}
