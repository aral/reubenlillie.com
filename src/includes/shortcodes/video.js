/**
 * @file Defines a shortcode for displaying embedded videos (youtube)
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#javascript-template-functions JavaScript template functions in 11ty}
 */

/**
 * A JavaScript Template module for embedded videos
 * @module includes/shortcodes/video
 * @param {Object} eleventyConfig 11ty’s Config API
 */
module.exports = function (eleventyConfig) {

  /**
   * Embedded video markup
   * @method
   * @name video
   * @param {Object} video The video data object
   * @return {String} HTML template literal
   * @example `${this.video(data)}`
   */
  eleventyConfig.addJavaScriptFunction('video', function (video) {
    return `<figure>
      <figcaption><h2>${video.title}</h2></figcaption>
      <iframe class="video" width="560" height="315" src="${video.src}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      <details>
        <summary class="border-bottom">
          <h3>Video Information</h3>
        </summary>
        ${video.details}
      </details>
    </figure>`
  })

}
