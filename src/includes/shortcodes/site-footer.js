/**
 * @file Defines a shortcode for the page footer markup
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#javascript-template-functions JavaScript template functions in 11ty}
 */

/**
 * A JavaScript Template module for the page footer
 * @module includes/shortcodes/site-footer
 * @param {Object} eleventyConfig 11ty’s Config API
 */
module.exports = function (eleventyConfig) {

  /**
   * The page footer markup
   * @method
   * @name siteFooter
   * @example `${this.siteFooter(data)}`
   * @param {Object} data 11ty’s data object
   * @return {String} HTML template literal
   */
  eleventyConfig.addShortcode('siteFooter', function (data) {
    return `<footer id="site_footer" class="grid grid-column-full-bleed small less-padding align-items-center">
        ${this.copyrightNotice(data)}
        ${this.gridNav(data.collections.policies, data.page)}
      </footer>`
  })

}
