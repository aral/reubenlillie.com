/**
 * @file Defines a shortcode for displaying block quotes (reviews, testimonials)
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#javascript-template-functions JavaScript template functions in 11ty}
 */

/**
 * A JavaScript Template module for block quotes
 * @module includes/shortcodes/blockquote
 * @param {Object} eleventyConfig 11ty’s Config API
 */
module.exports = function (eleventyConfig) {

  /**
   * Blockquote markup
   * @method
   * @name blockquote
   * @param {Object} quote The quote to display
   * @return {String} HTML template literal
   * @example `${this.blockquote(data)}`
   */
  eleventyConfig.addShortcode('blockquote', function (quote) {
    return `<blockquote class="flex flex-column quote">
      <p class="no-margin">${quote.text}</p>
      <cite class="align-self-end small">—<a href="${quote.src}">${quote.cite}</a></cite>
    </blockquote>`
  })

}
