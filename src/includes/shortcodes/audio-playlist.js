/**
 * @file Defines a shortcode for displaying an embedded audio playlist
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#javascript-template-functions 11ty docs}
 */

/**
 * A JavaScript Template module for an embedded `<audio>` playlist
 * @module includes/shortcodes/audio-playlist
 * @param {Object} eleventyConfig 11ty’s Config API
 */
module.exports = function (eleventyConfig) {

  /**
   * Playlist markup
   * @method
   * @name audioPlaylist
   * @param {Object} audio The audio data object
   * @return {String} HTML template literal
   * @example `${this.audioPlaylist(data.audio.playlists.songsOfTravel)}`
   */
  eleventyConfig.addShortcode('audioPlaylist', function (playlist) {
    return `<figure class="playlist">
      <figcaption><h2>${playlist.title}</h2></figcaption>
      <details>
        <summary class="border-bottom"><h3>Playlist Information</h3></summary>
        ${playlist.details}
      </details>
      ${playlist.tracks
        .map(function (track) {
          return `<h3>${track.title}</h3>
            <audio controls>
              <source src="${track.src.ogg}" type="audio/ogg">
              <source src="${track.src.mp3}" type="audio/mpeg">
            </audio>
            <details>
              <summary class="border-bottom">
                <h4>Track Information</h4>
              </summary>
              ${track.details}
            </details>`
        }).join("\n")
      }
    </figure>`
  })

}
