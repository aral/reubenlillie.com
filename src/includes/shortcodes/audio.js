/**
 * @file Defines a shortcode for displaying embedded `<audio>`
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#javascript-template-functions JavaScript template functions in 11ty}
 */

/**
 * A JavaScript Template module for embedded `<audio>`
 * @module includes/shortcodes/audio
 * @param {Object} eleventyConfig 11ty’s Config API
 */
module.exports = function (eleventyConfig) {

  /**
   * Embedded audio markup
   * @method
   * @name audio
   * @param {Object} audio The audio data object
   * @return {String} HTML template literal
   * @example `${this.audioPlaylist(data.audio(track)}`
   */
  eleventyConfig.addShortcode('audio', function (track) {
    return `<h3>${track.title}</h3>
      <audio controls>
        <source src="${track.src.ogg}" type="audio/ogg">
        <source src="${track.src.mp3}" type="audio/mpeg">
      </audio>
      <details>
        <summary class="border-bottom">
          <h4>Track Information</h4>
        </summary>
        ${track.details}
      </details>`
  })

}

