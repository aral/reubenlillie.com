/*
 * @file Declares critical CSS to minify and load inline
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/***********************************************************************
 * Table of Contents
 *
 * 1.0 Custom Fonts
 * 2.0 Document-Level Rules
 *   2.1 Custom Properties
 *   2.2 User Interactions
 * 3.0 Layout
 *   3.1 Grid
 *   3.2 Flexbox
 * 4.0 Images
 * 5.0 Typography
 * 6.0 Links
 * 7.0 Colors, Borders, and Shading
 *   7.1 Colors
 *   7.2 Borders
 *   7.3 Shading
 **********************************************************************/

/***********************************************************************
 * 1.0 Custom Fonts
 *
 * @see {@link https://developer.mozilla.org/en-US/docs/Web/CSS/@font-face}
 * @see {@link https://www.zachleat.com/web/comprehensive-webfonts/}
 **********************************************************************/

/*
 *  Spiffy Sans
 *  SIL Open Font License v1.10
 */

@font-face {
  font-family: Spiffy Sans;
  src: url('/includes/assets/fonts/spiffy-sans/SpiffySans-Light.woff2') format('woff2'),
    url('/includes/assets/fonts/spiffy-sans/SpiffySans-SemiBold.woff') format('woff');
  font-style: normal;
}

@font-face {
  font-family: Spiffy Sans;
  src: url('/includes/assets/fonts/spiffy-sans/SpiffySans-LightItalic.woff2') format('woff2'),
    url('/includes/assets/fonts/spiffy-sans/SpiffySans-LightItalic.woff') format('woff');
  font-style: italic;
}

@font-face {
  font-family: Spiffy Sans;
  src: url('/includes/assets/fonts/spiffy-sans/SpiffySans-SemiBold.woff2') format('woff2'),
    url('/includes/assets/fonts/spiffy-sans/SpiffySans-SemiBold.woff') format('woff');
  font-weight: bold;
}

/***********************************************************************
 * 2.0 Document-Level Rules
 **********************************************************************/

/*
 * 2.1 Custom Properties
 *
 * @see {@link https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties}
 */

:root {
  /* Units */
  --base-unit: 1vw;
  --base-unit-sixteenth: calc(var(--base-unit, 1vw) / 16);
  --base-unit-eighth: calc(var(--base-unit, 1vw) / 8);
  --base-unit-quarter: calc(var(--base-unit, 1vw) / 4);
  --base-unit-half: calc(var(--base-unit, 1vw) / 2);
  --base-unit-2x: calc(var(--base-unit, 1vw) * 2);
  --base-unit-4x: calc(var(--base-unit, 1vw) * 4);
  --paragraph-width: 66ch;
  /* Colors */
  /* Tiger Tail: PPG1198-6; Pantone 7412 C/7571 C */
  --primary-color: #ce7639;
  --black: #271b0f;
  --gray: #848484;
  --white: white;
  --body-text: var(--black, #271b0f);
  --body-background: var(--white, white);
  /* Shading */
  --shadow: var(--base-unit-eighth, 0.125vw)
            var(--base-unit-eighth, 0.125vw)
            var(--base-unit-eighth, 0.125vw)
            rgba(15,11,6,0.25);
}

/*
 * 2.2 User Interactions
 */

html {
  /*
   * Use smooth scrolling to page anchors by default
   * @see {@link https://developer.mozilla.org/en-US/docs/Web/CSS/scroll-behavior}
   * @see {@link https://gomakethings.com/smooth-scrolling-links-with-only-css/}
   */
  scroll-behavior: smooth;
}

@media screen and (prefers-reduced-motion: reduce) {
  html {
    scroll-behavior: auto;
  }
}

/*
 * Show pointer for clickable elements
 * @see {@link https://developer.mozilla.org/en-US/docs/Web/CSS/cursor}
 */
button:hover,
summary:hover {
  cursor: pointer;
}

/***********************************************************************
 * 3.0 Layout
 **********************************************************************/

/*
 * Universal box-sizing
 * @see {@link https://developer.mozilla.org/en-US/docs/Web/CSS/box-sizing}
 */
*,
*:before,
*:after {
  box-sizing: border-box;
}

/*
 * 3.1 Grid
 *
 * @see {@link https://developer.mozilla.org/en-US/docs/Glossary/Grid}
 */

body {
  grid-template-columns: 1fr minmax(320px, var(--paragraph-width, 66ch)) 1fr;
  grid-template-rows: min-content 1fr min-content;
  grid-row-gap: var(--base-unit-2x, 2vw);
  min-height: 100vh;
}

.grid {
  display: grid;
}

.grid-column-full-bleed {
  grid-column: 1 / -1;
}

.grid-column-content {
  grid-column: 2;
}

#site_header {
  grid-row: 1;
}

#main {
  grid-row: 2;
}

#site_footer {
  grid-row: 3;
  grid-template-columns: 1fr max-content;
}

#site_header nav .nav-item,
#site_footer nav .nav-item {
  grid-row: 1;
}

#main nav {
  display: flex;
  flex-wrap: wrap;
}

#site_footer nav {
  grid-template-columns: repeat(2,minmax(min-content, var(--base-unit-4x, 4vw)));
}

/*
 * 3.2 Flexbox
 *
 * @see {@link https://developer.mozilla.org/en-US/docs/Glossary/Flexbox}
 */

.flex {
  display: flex;
}

.flex-column {
  flex-direction: column;
}

.flex-wrap {
  flex-wrap: wrap;
}

/* Justification and Alignment */
.space-around {
  justify-content: space-around;
}

.space-between {
  justify-content: space-between;
}

.justify-center {
  justify-content: center;
}

.align-items-center {
  align-items: center;
}

.align-self-end {
  align-self: end;
}

.align-right {
  text-align: right;
}

/*
 * Floats
 *
 * Center content on smallest screens
 */
.float {
  display: flex;
  justify-content: center;
}

/*
 * Employ floats on larger screens
 */
@media screen and (min-width: 480px) {
  .float-left {
    float: left;
    margin-right: 1em;
    margin-bottom: 1em;
  }
}

/* Padding */
.padding {
  padding: 1vw;
  padding: var(--base-unit, 1vw);
}

.less-padding {
  padding: 0.25vw;
  padding: var(--base-unit-quarter, 0.25vw);
}

.more-padding {
  padding: 4vw;
  padding: var(--base-unit-4x, 4vw);
}

.no-padding-left {
  padding-left: 0;
}

/* Margin */
.no-margin {
  margin: 0;
}

.margin-bottom {
  margin-bottom: 1em;
}

/***********************************************************************
 * 4.0 Images
 **********************************************************************/

picture {
  line-height: 0;
  width: max-content;
  margin: 0 auto;
}

img {
  color: lightgrey;
  height: auto;
  max-width: 100%;
}

#site_header .headshot {
  height: 1.5em;
}

nav > a {
  text-decoration: none;
}

/***********************************************************************
 * 5.0 Typography
 **********************************************************************/

html {
  color: #271b0f;
  color: var(--body-text, #271b0f);
  font-family: Spiffy Sans, sans-serif;
  line-height: 1.5;
}

@media screen and (min-width: 320px) {
  html {
    /* Increase font size from smaller screens */
    font-size: 1.25em;
  }
}

@media screen and (min-width: 1100px) {
  html {
    /* Size fonts dynamically on larger screens */
    font-size: 1.75vw;
  }
}

p {
  max-width: 33em;
  max-width: var(--paragraph-width, 66ch);
}

@supports (display: grid) {
  p {
    max-width: inherit;
  }
}

.bold {
  font-weight: bold;
}

.italic {
  font-style: italic;
}

.x-small {
  font-size: x-small;
}

.small {
  font-size: small;
}

.medium {
  font-size: medium;
}

.larger {
  font-size: larger;
}

.x-large,
.xx-large {
  font-size: x-large;
}

@media screen and (min-width: 600px) {
  .xx-large {
    /* Scale up largest font sizes on larger screens */
    font-size: xx-large;
  }
}

.hanging-indent {
  padding-left: 1em;
  padding-left: 2ch;
  text-indent: -1em;
  text-indent: -2ch;
}

.hanging-indent > * {
  margin-bottom: 1em;
}

.indent {
  text-indent: 1em;
  text-indent: 2ch;
}

.list-inside {
  list-style-position: inside;
}

.no-list-style {
  list-style: none;
}

.text-center {
  text-align: center;
}

/***********************************************************************
 * 6.0 Links
 **********************************************************************/

a {
  color: #ce7639;
  color: var(--primary-color, #ce7639);
  font-weight: bold;
}

.nav-item {
  position: relative;
}

.nav-item:before {
  background-color: #ce7639;
  background-color: var(--primary-color, #ce7639);
  bottom: 1vw;
  bottom: var(--base-unit, 1vw);
  content: '';
  height: 1.87px;
  left: 0;
  position: absolute;
  transform: scaleX(0);
  transform-origin: bottom right;
  transition: transform 0.25s ease-in-out;
  width: 100%;
}

.nav-item:not(.active):focus:before,
.nav-item:not(.active):hover:before {
  transform: scaleX(1);
  transform-origin: bottom left;
}

/***********************************************************************
 * 7.0 Colors, Borders, and Shading
 **********************************************************************/

/*
 * 7.1 Colors
 */

body {
  background: transparent;
}

@media (prefers-color-scheme: dark) {
  :root {
    --body-text: var(--white, #fff);
    --body-background: var(--black, #271b0f);
  }

  body {
    background: #271b0f;
    background: var(--black, #271b0f);
  }
}

nav {
  min-height: 2.5em;
}

.nav-item {
  padding: 1vw;
  padding: var(--base-unit, 1vw);
  align-self: center;
  justify-items: center;
  text-align: center;
}

#site_header nav {
  justify-content: space-between;
  padding: 0.5vw;
  padding: var(--base-unit-half, 0.5vw);
}

#main nav {
  justify-content: center;
}

.active {
  color: #848484;
  color: var(--gray, #848484);
}

.primary-color {
  color: #ce7639;
  color: var(--primary-color, #ce7639);
}

/*
 * 7.2 Borders
 */

.border-radius {
  border-radius: 0.5vw;
  border-radius: var(--base-unit-half, 0.5vw);
}

.circular {
  border-radius: 50%;
}

/*
 * 7.3 Shading
 */

.shadow {
  box-shadow: 0.125vw 0.125vw 0.125vw rgba(15,11,6,0.25);
  box-shadow: var(--shadow, 0.125vw 0.125vw 0.125vw rgba(15,11,6,0.25));
}
