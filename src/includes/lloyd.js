/**
 * @file Imports theme modules and configures them with 11ty (~/.eleventy.js)
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

// Import theme filters
var fileToString = require('./filters/file-to-string')
var minifyCSS = require('./filters/minify-css')
var minifyJS = require('./filters/minify-js')

// Import theme shortcodes
var audio = require('./shortcodes/audio')
var audioPlaylist = require('./shortcodes/audio-playlist')
var authorMeta = require('./shortcodes/author-meta')
var blockquote = require('./shortcodes/blockquote')
var copyrightNotice = require('./shortcodes/copyright-notice')
var description = require('./shortcodes/description')
var externalCSS = require('./shortcodes/external-css')
var favicon = require('./shortcodes/favicon')
var gridNav = require('./shortcodes/grid-nav')
var headTag = require('./shortcodes/head-tag')
var pageDate = require('./shortcodes/page-date')
var siteHeader = require('./shortcodes/site-header')
var siteFooter = require('./shortcodes/site-footer')
var socialMeta = require('./shortcodes/social-meta')
var titleTag = require('./shortcodes/title-tag')
var video = require('./shortcodes/video')

// Import theme transforms
var minifyHTML = require('./transforms/minify-html')

/**
 * A loader module for theme modules
 * @module includes/lloyd
 */
module.exports = function (eleventyConfig) {

  // Function calls to included filters
  fileToString(eleventyConfig)
  minifyCSS(eleventyConfig)
  minifyJS(eleventyConfig)

  // Function calls to included shortcodes
  audio(eleventyConfig)
  audioPlaylist(eleventyConfig)
  authorMeta(eleventyConfig)
  blockquote(eleventyConfig)
  copyrightNotice(eleventyConfig)
  description(eleventyConfig)
  externalCSS(eleventyConfig)
  favicon(eleventyConfig)
  gridNav(eleventyConfig)
  headTag(eleventyConfig)
  pageDate(eleventyConfig)
  siteHeader(eleventyConfig)
  siteFooter(eleventyConfig)
  socialMeta(eleventyConfig)
  titleTag(eleventyConfig)
  video(eleventyConfig)

  // Function calls to included transforms
  minifyHTML(eleventyConfig)

  return

}
