/**
 * @file Defines the chained template for academic papers
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in 11ty}
 */

/**
 * Acts as front matter in JavaScript templates.
 */
exports.data = {
  layout: 'layouts/content'
}

/**
 * The content of the template
 * @method
 * @name render()
 * @param {Object} data 11ty’s data object
 * @return {String} HTML template literal
 * @see {@link https://www.11ty.dev/docs/shortcodes/ 11ty shortcodes}
 */
exports.render = function (data) {
  return  `<p>Here’s a sample of papers I’ve presented publicly.</p>
  <p>You can find full text copies, abastacts, and other information on <a href="https://olivet.academia.edu/ReubenLillie">Academia.edu</a>.</p>
  <p><em>Arranged by date</em></p>
  <ul class="hanging-indent no-list-style">
    ${data.writing.papers.map(paper => `<li>
      ${paper.author ? `${paper.author}.` : ''}
      ${paper.title ? `“${paper.title}.”` : ''}
      ${paper.format ? `${paper.format}.` : ''}
      ${paper.sponsor ? `${paper.sponsor},` : ''}
      ${paper.location ? `${paper.location},` : ''}
      ${paper.date ? `${paper.date}.` : ''}
      ${paper.url ? `<a href="${paper.url}">${paper.url}</a>.` : ''}
    </li>`
    ).join('')}
  </ul>`
}
