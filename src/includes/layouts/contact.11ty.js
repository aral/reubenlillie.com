/**
 * @file Defines the chained template for the contact page
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in 11ty}
 */

/**
 * Acts as front matter in JavaScript templates
 */
exports.data = {
  layout: 'layouts/base'
}

/**
 * The content of the template
 * @method
 * @name render()
 * @param {Object} data 11ty’s data object
 * @return {String} HTML template literal
 * @see {@link https://www.11ty.dev/docs/data-global/ Using data in 11ty}
 * @see {@link https://www.11ty.dev/docs/shortcodes/ 11ty shortcodes}
 */
exports.render = function (data) {
  return  `<header>
    <h1 class="flex justify-center">${data.site.title}</h1>
  </header>
  <aside class="text-center">
    <p>Thank you for contacting me!</p>
    <p>Send me a message with this form or through one of these social media platforms, <br>and I’ll get back to you soon.</p>
  </aside>
  <nav class="grid">
    ${data.site.author.social.accounts.map(account => `<a class="nav-item bold" href="${account.url}">
      ${account.name}
    </a>`).join('')}
  </nav>
  <form id="contact"
        class="flex flex-column"
        method="POST"
        action="/thanks/"
        autocomplete="on"
        netlify-honepot="_honeypot"
        data-netlify="true">
    <fieldset class="flex flex-column border-radius padding">
      <legend>Email Me</legend>
      <input name="_honeypot"
             type="text"
             style="display:none">
      <label for="name">Name</label>
      <input id="name"
             name="name"
             type="text" name="name"
             required="true"
             autofocus="true">
      <label for="email">Email</label>
      <input id="email"
             name="email"
             type="email"
             required="true">
      <label for="message">Message</label>
      <textarea id="message"
                name="message"
                rows="13"
                cols="47"></textarea>
      <button id="submit"
              class="align-self-end"
              name="submit"
              type="submit">Send</button>
    </fieldset>
  </form>`
}
