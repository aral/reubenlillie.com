/**
 * @file Defines the chained template for coding projects
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in 11ty}
 */

/**
 * Acts as front matter in JavaScript templates.
 */
exports.data = {
  layout: 'layouts/content'
}

/**
 * The content of the template
 * @method
 * @name render()
 * @param {Object} data 11ty’s data object
 * @return {String} HTML template literal
 * @see {@link https://www.11ty.dev/docs/shortcodes/ 11ty shortcodes}
 */
exports.render = function (data) {
  return  `<p>In addition to <a href="https://gitlab.com/reubenlillie/reubenlillie.com/">this site’s source code</a>, here’s a sample of coding projects to which I’ve contributed.</p>
  <p>Check out the code on <a href="https://github.com/reubenlillie/">GitHub</a> and <a href="https://gitlab.com/reubenlillie/">GitLab</a>.</a>
  <p><em>Arranged by name</em></p>
    ${data.coding.projects.map(project => `
      ${project.name ? `<h2>${project.name}</h2>` : ''}
      ${project.url ? `<p><a href="${project.url}">${project.url}</a></p>` : ''}
      ${project.description ? `<p>${project.description}</p>` : ''}`
    ).join('')}`
}
