/**
 * @file Defines the nested template for audio and video
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in 11ty}
 */

/**
 * Acts as front matter in JavaScript templates
 */
exports.data = {
  layout: 'layouts/content'
}

/**
 * Renders the template markup
 * @method
 * @name render()
 * @param {Object} data 11ty’s data object
 * @return {String} HTML template literal
 * @see {@link https://www.11ty.dev/docs/shortcodes/ 11ty docs}
 */
exports.render = function (data) {
  return  `${this.blockquote(
    data.singing.reviews.operaToday.glassGalileoGalileiDesMoinesMetroOpera2016
  )}
  ${data.content}
  ${this.video(data.singing.videos.hagenAmeliaLetterAriaCCNATS)}
  ${this.blockquote(
    data.singing.reviews.operaToday.morrisonOscarTheSanteFeOpera2013
  )}
  ${this.audioPlaylist(data.singing.audio.playlists.songsOfTravel)}`
}
