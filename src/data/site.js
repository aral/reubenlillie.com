/**
 * @file Contains global metadata for configuring the site
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/**
 * Global site data module.
 * @module data/site
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data files in 11ty}
 */
module.exports = {
  title: "Reuben L. Lillie",
  tagline: "Philosopher, Musician, Theologian",
  copyright: {
    year: new Date().getFullYear,
    holder: "Reuben L. Lillie",
    license: {
      abbr: "CC BY-SA",
      name: "Creative Commons Attribution-ShareAlike 4.0 International license",
      url: "https://creativecommons.org/licenses/by-sa/4.0/"
    }
  },
  author: {
    name: {
      fullName: "Reuben L. Lillie",
      givenName: "Reuben",
      surname: "Lillie"
    },
    social: {
      accounts: [
        {
          name: "Academia.edu",
          url: "https://olivet.academia.edu/ReubenLillie/"
        },
        {
          name: "GitHub",
          url: "https://github.com/reubenlillie/"
        },
        {
          name: "GitLab",
          url: "https://gitlab.com/reubenlillie/"
        },
        {
          name: "Instagram",
          url: "https://www.instagram.com/reuben.lillie/"
        },
        {
          name: "LinkedIn",
          url: "https://linkedin.com/in/reubenlillie/"
        },
        {
          name: "Messenger",
          url: "https://m.me/reubenlillie/"
        },
        {
          name: "Twitter",
          url: "https://twitter.com/reubenlillie/"
        }
      ]
    }
  },
  locale : {
    code: 'en-US'
  },
  color: {
    main: "#ce7639",
  }
}
