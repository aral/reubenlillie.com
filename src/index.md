---
title: Hi, I’m Reuben.
shortTitle: Reuben L. Lillie
layout: layouts/home
tags: headerMenu
weight: 1
description: I’m a philosopher, musician, and theologian. I’m available for singing, speaking, writing, coding, teaching, and parliamentary engagements.
---

