/**
 * @file Configures 11ty options and helper methods
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

// Require 11ty’s syntax highlighting plugin
var syntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight")

// Require the theme module
var lloyd = require('./src/includes/lloyd.js')

/**
 * 11ty’s configuration module
 * @module .eleventy
 * @param {Object} eleventyConfig 11ty’s Config API
 * @return {Object} {} 11ty’s optional Config object
 * @see {@link https://www.11ty.dev/docs/config/}
 */
module.exports = function (eleventyConfig) {

  // Pass 11ty’s Conif object to the theme configuration file (~/src/includes)
  lloyd(eleventyConfig)

  /**
   * Defines a collection for all pages
   * @param {Object} collection 11ty’s collection object
   * @param {String} name The name of the collection
   * @return {Object} The filtered collection
   * @see {@link https://www.11ty.dev/docs/collections/ 11ty docs}
   */
  eleventyConfig.addCollection('pages', function (collection) {
    return collection.getFilteredByGlob('src/pages/*.md')
  })

  /**
   * Copies static assets to the output directory
   * @see {@link https://www.11ty.dev/docs/copy/ 11ty docs}
   */
  eleventyConfig.addPassthroughCopy('src/includes/assets')
  eleventyConfig.addPassthroughCopy('src/favicons')

  /**
   * Adds syntax highlighting for code snippets
   * @see {@link https://github.com/11ty/eleventy-plugin-syntaxhighlight Plugin repo on GitHub}
   */
  eleventyConfig.addPlugin(syntaxHighlight, {
    templateFormats: ['md']
  })

  /*
   * 11ty’s optional Config object
   */
  return {
    dir: {
      data: 'data', // relative to input
      includes: 'includes', // relative to input
      input: 'src',
      output: 'dist'
    }
  }

}
